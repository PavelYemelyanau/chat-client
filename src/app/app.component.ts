import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from './shared/constants/local-storage.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  constructor(private router: Router) {}

  public ngOnInit(): void {
    const token = localStorage.getItem(LocalStorage.Token);
    console.log(token);
    if (token) {
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('/start/auth');
    }
  }
}
