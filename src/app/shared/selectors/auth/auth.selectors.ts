import { createSelector } from '@ngrx/store';
import { AppState } from '../../reducers';
import { AuthState } from '../../reducers/auth/auth.reducer';

const getAuthState = (state: AppState): AuthState => state.auth;

export const getAuthFormDisabled = createSelector(
  getAuthState,
  (authState: AuthState) => authState.formDisabled
);
