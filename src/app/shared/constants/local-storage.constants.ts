export enum LocalStorage {
  Token = 'USER_TOKEN',
  Host = 'HOST',
  Expires_at = 'EXPIRES_AT',
}
