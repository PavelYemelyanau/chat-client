import { LocalStorage } from './../constants/local-storage.constants';
import {
  environment,
  authorization,
} from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser } from '../interfaces/user/user.interface';
import { IToken } from '../interfaces/auth/token/token.interface';
import { LoginForm } from '../interfaces/auth/login/login.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  public authorize(data: LoginForm): Observable<IToken> {
    return this.http.post<IToken>(
      `${environment.dbUrl}/${authorization.baseUrl}/signIn`,
      data
    );
  }

  public sigUp(user: IUser): Observable<IToken> {
    return this.http.post<IToken>(
      `${environment.dbUrl}/${authorization.baseUrl}/signUp`,
      user
    );
  }

  public checkSession(): boolean {
    const exp = localStorage.getItem(LocalStorage.Expires_at);
    if (exp && exp !== 'undefined') {
    } else {
      return false;
    }
  }

  public setSession(): any {}
}
