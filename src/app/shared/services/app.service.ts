import { AuthService } from './auth.service';
import { LocalStorage } from './../constants/local-storage.constants';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private authService: AuthService) {}

  public getHost(): string {
    return localStorage.getItem(LocalStorage.Host) || environment.dbUrl;
  }

  public getToken(): string {
    return localStorage.getItem(LocalStorage.Token);
  }
}
