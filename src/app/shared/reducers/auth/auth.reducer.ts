import {
  loginSuccessAction,
  loginFailedAction,
  signUpFailedAction,
} from './../../actions/auth/auth.actions';
import { Action, createReducer, on } from '@ngrx/store';
import {
  loginAction,
  signUpAction,
} from '../../actions/auth/auth.actions';

const initialState: AuthState = {
  jwtToken: null,
  formDisabled: false,
};

export interface AuthState {
  jwtToken: string;
  formDisabled: boolean;
}

const reducer = createReducer<AuthState>(
  initialState,
  on(loginAction, signUpAction, (state: AuthState) => ({
    ...state,
    formDisabled: true,
  })),
  on(loginSuccessAction, (state, action) => ({
    ...state,
    jwtToken: action.payload,
    formDisabled: false,
  })),
  on(loginFailedAction, signUpFailedAction, (state) => ({
    ...state,
    formDisabled: false,
  }))
);

export function authReducer(
  state: AuthState = initialState,
  action: Action
): AuthState {
  return reducer(state, action);
}
