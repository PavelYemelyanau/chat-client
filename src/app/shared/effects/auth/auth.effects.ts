import {
  loginSuccessAction,
  loginFailedAction,
} from './../../actions/auth/auth.actions';
import { IToken } from './../../interfaces/auth/token/token.interface';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  AuthActionsTypes,
  AuthActionsUnion,
} from '../../actions/auth/auth.actions';
import { Action } from '@ngrx/store';

@Injectable()
export class AuthEffects {
  public login$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActionsTypes.Login),
      switchMap(({ payload }) =>
        this.authService.authorize(payload).pipe(
          map((token: IToken) =>
            loginSuccessAction({ payload: token })
          ),
          catchError((error) =>
            of(
              loginFailedAction({ payload: { message: error.message } })
            )
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions<AuthActionsUnion>,
    private authService: AuthService
  ) {}
}
