import { Observable } from 'rxjs';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppService } from '../services/app.service';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  private baseUrl: string;
  constructor(private appService: AppService) {
    this.baseUrl = this.appService.getHost();
  }

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const clone = request.clone({ url: this.baseUrl + request.url });
    return next.handle(clone);
  }
}
