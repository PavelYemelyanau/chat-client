import { createAction, union, props } from '@ngrx/store';
import { LoginForm } from '../../interfaces/auth/login/login.interface';
import { IUser } from '../../interfaces/user/user.interface';

export enum AuthActionsTypes {
  Login = '[AUTH] LOGIN_ACTION',
  LoginSuccess = '[AUTH] LOGIN_SUCCESS_ACTION',
  LoginFailed = '[AUTH] LOGIN_FAILED_ACTION',

  SignUp = '[AUTH] SIGN_UP_ACTION',
  SignUpSuccess = '[AUTH] SIGN_UP_SUCCESS_ACTION',
  SignUpFailed = '[AUTH] SIGN_UP_FAILED_ACTION',
}

export const loginAction = createAction(
  AuthActionsTypes.Login,
  props<{ payload: LoginForm }>()
);

export const loginSuccessAction = createAction(
  AuthActionsTypes.LoginSuccess,
  props<{ payload: any }>()
);

export const loginFailedAction = createAction(
  AuthActionsTypes.LoginFailed,
  props<{ payload: { message: string } }>()
);

export const signUpAction = createAction(
  AuthActionsTypes.SignUp,
  props<{ payload: IUser }>()
);

export const signUpSuccessAction = createAction(
  AuthActionsTypes.SignUpSuccess,
  props<{ payload: any }>()
);

export const signUpFailedAction = createAction(
  AuthActionsTypes.SignUpFailed,
  props<{ payload: { message: string } }>()
);

const all = union({
  loginAction,
  loginSuccessAction,
  loginFailedAction,
  signUpAction,
  signUpSuccessAction,
  signUpFailedAction,
});

export type AuthActionsUnion = typeof all;
