import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FormInputComponent),
      multi: true,
    },
  ],
})
export class FormInputComponent implements ControlValueAccessor {
  @Input()
  public placeholder: string = '';
  @Input()
  public label = '';
  @Input()
  public value: string;
  @Input()
  public type: string = 'text';
  @Input()
  public errors: any;

  public onChange: (data: any) => void = () => {};
  public onTouched: () => void = () => {};
  public isTouched = false;
  public isDisabled = false;

  constructor(private cdRef: ChangeDetectorRef) {}

  public setTouched(): void {
    this.isTouched = true;
    this.onTouched();
  }

  public changeValue(event: any): void {
    this.writeValue(event.target.value);
  }

  public writeValue(value: any): void {
    this.onChange(value);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.cdRef.markForCheck();
  }
}
