import { Observable } from 'rxjs';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { loginAction } from 'src/app/shared/actions/auth/auth.actions';
import { AppState } from 'src/app/shared/reducers';
import { getAuthFormDisabled } from 'src/app/shared/selectors/auth/auth.selectors';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AuthPage implements OnInit {
  public form: FormGroup;
  public formDisabled: boolean;
  constructor(private store$: Store<AppState>) {}

  public ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.email,
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
      ]),
    });
    this.store$.select(getAuthFormDisabled).subscribe((flag) => {
      console.log(flag);
      this.formDisabled = flag;
    });
  }

  public signIn(): void {
    this.form.disable();
    this.store$.dispatch(loginAction({ payload: this.form.value }));
  }
}
