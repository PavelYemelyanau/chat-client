import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthPage } from './pages/auth/auth.page';

const routes: Routes = [{ path: 'auth', component: AuthPage }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartRoutingModule {}
